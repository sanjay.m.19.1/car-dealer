function problem1(inventory, searchId){
    for(let car = 0; car < inventory.length; car++){
        if(inventory[car]["id"] == searchId)
        return `Car ${searchId} is a ${inventory[car]['car_year']} ${inventory[car]['car_make']} ${inventory[car]['car_model']}`;
    }
    return `No cars matching id ${searchId}`;
}
module.exports = {problem1 : problem1};