// takes dealer data and car_make as list
function problem6(inventory, preferance){
    var result = [];
    for(let car = 0; car < inventory.length; car++){
        if(preferance.includes(inventory[car]['car_make'])){
            result.push(inventory[car]);
        }
    }
    return result.length == 0 ? "no cars match" : JSON.stringify(result);
}

module.exports = {problem6 : problem6};