const inventory = require('../cars.js');
const { problem5 } = require('../problem5');

let olderThan = 2000; //the year after which cars should be searched.
let result = problem5(inventory, olderThan); 
console.log(result.length);