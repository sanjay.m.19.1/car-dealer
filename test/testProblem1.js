const inventory = require('../cars.js');
const { problem1 } = require('../problem1');

let searchId = 33;  // id that has to be searched
console.log(problem1(inventory, searchId)); // takes dealer data and id of a car as parameters