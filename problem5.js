const { problem4 } = require('./problem4');

// takes dealer data and year as parameters
function problem5(inventory, olderThan){
    var year = problem4(inventory);
    var result = [];
    for(let car = 0; car < year.length; car++){
        if(year[car] < olderThan){
            result.push(inventory[car]);  // adding car details to result if its corresponding year matches query
        }
    }
    return result;
}

module.exports = {problem5 : problem5};