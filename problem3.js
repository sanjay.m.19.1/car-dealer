function problem3(inventory){
    function compare(a, b) {             //callback function to sort alphabetically.
        if (a.car_model < b.car_model){
          return -1;
        }
        if (a.car_model > b.car_model){
          return 1;
        }
        return 0;
      }
      inventory.sort(compare);
      var result = [];
      for(let car = 0; car < inventory.length; car++){
          result.push(inventory[car]['car_model']);
      }
      if(result.length == 0){
        return "Lot is empty";
      }
      return result.join('\n');
    
}

module.exports = {problem3 : problem3};