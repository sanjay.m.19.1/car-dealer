function problem2(inventory){
    if (inventory.length == 0){
        return "Lot is empty"
    }
    let result = inventory.slice(-1)[0];
    return `Last car is a ${result['car_make']} ${result['car_model']}`
}

module.exports = {problem2 : problem2};