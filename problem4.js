function problem4(inventory){
    var result = [];
    for(let car = 0; car < inventory.length; car++){
        result.push(inventory[car]['car_year']);
    }
    return result;
}

module.exports = {problem4 : problem4};